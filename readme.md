# Basic Celery with Redis and Docker

## Overview

* Web applications need to handle tasks efficiently in order to scale and operate optimally.
* Celery and Redis are popular open source tools that combine to provide queuing and task execution, optimising app operations.
* This basic app is designed to show [Redis](https://redis.io/) and [Celery](https://docs.celeryproject.org/en/stable/) operating together on [Docker](https://docs.docker.com/) containers.

* There is no app content, it is purely an example of celery operating simply in the terminal

## How to run it yourself
* clone this repo onto your machine (note: this was built on MacOS)
* docker-compose build
* docker-compose up
* Ctrl+C to stop the container, then docker-compose down to remove the containers

## OUTPUT
* Docker-Compose spins up a Redis Server, then creates a celery worker which connects to redis and schedules the message "Hi" to print every 10 seconds to the terminal.
* celery/celeryconfig.py contains the scheduling (celery "beat"), celery/tasks.py contains the task and celery/worker.py runs it.

## Differences from virtual env version ([link here](https://bitbucket.org/GeorgeGoldberg/celery_venv)):
- celery files are neatly tucked into a folder
- broker_url in celeryconfig.py changed from redis://localhost/0 to redis://redis:7379/0
- redis added to requirements.txt and docker-compose file in order to start the server for celery

### Notes:

- use `-l info` in the start.sh file to see more info on celery in the terminal when it's running.
- app.py file is empty but can be configured by uncommenting out the flask code and adding folders for the web content
