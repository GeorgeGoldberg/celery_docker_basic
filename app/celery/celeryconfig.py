#import tasks and set up scheduler
imports = ('tasks')
task_ignore_result = False

result_backend = 'redis://redis:6379/0'
broker_url = 'redis://redis:6379/0'

from datetime import timedelta

beat_schedule = {
    'say_hi_10s_intervals':{
        'task':'tasks.say_hi',
        'schedule':timedelta(seconds=10)
    }
}
